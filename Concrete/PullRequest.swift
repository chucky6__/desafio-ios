//
//  PullRequest.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    var owner: Owner?
    var title: String?
    var description: String?
    var state: String?
    var url: String?
    var date: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        owner           <- map["user"]
        title           <- map["title"]
        description     <- map["body"]
        state           <- map["state"]
        url             <- map["html_url"]
        date            <- map["created_at"]
    }
    
}