//
//  MainSplitViewController.swift
//  Concrete
//
//  Created by Antonio Alves on 5/10/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit
    
class MainSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    }
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool{
        return true
    }
        
}
