//
//  Repository.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var owner: Owner?
    var name: String?
    var description: String?
    var stars: Int?
    var forks: Int?
    
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        name            <- map["name"]
        description     <- map["description"]
        stars           <- map["stargazers_count"]
        forks           <- map["forks"]
        owner           <- map["owner"]
    }
}
