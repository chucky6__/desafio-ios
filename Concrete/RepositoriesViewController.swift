//
//  ViewController.swift
//  Concrete
//
//  Created by Antonio Alves on 5/7/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit
import ObjectMapper

class RepositoriesViewController: UIViewController {
    
    var page = 1
    var repositories: [Repository] = []
    var firstTime = true
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var cellNib = UINib(nibName: TableViewCellIdentifiers.repositoryCell, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.repositoryCell)
        cellNib = UINib(nibName: TableViewCellIdentifiers.loadingCell, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.loadingCell)
        
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.automaticallyAdjustsScrollViewInsets = false

        
        getRepos()
    }
    
    private func getRepos() {
        GitHub.getRepositories(page) { (json, error) in
            if error != nil {
                print(error)
            }
            self.firstTime = false
            let repos = Mapper<Repositories>().map(json)
            self.repositories.appendContentsOf(repos!.repositories!)
            self.page = self.page + 1
            self.tableView.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPullRequests" {
            if let nav = segue.destinationViewController as? UINavigationController {
                let controller = nav.viewControllers.first as! PullRequestsViewController
                if let index = sender as? NSIndexPath {
                    let repo = repositories[index.row]
                    controller.repository = repo
                }
            }
        }
    }
    

}

extension RepositoriesViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if firstTime {
            return 1
        }
        return repositories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if firstTime {
            return tableView.dequeueReusableCellWithIdentifier(TableViewCellIdentifiers.loadingCell, forIndexPath: indexPath)
        }
        if indexPath.row == repositories.count - 1 {
            getRepos()
            return tableView.dequeueReusableCellWithIdentifier(TableViewCellIdentifiers.loadingCell, forIndexPath: indexPath)
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(TableViewCellIdentifiers.repositoryCell, forIndexPath: indexPath) as! RepositoryCell
            let repo = repositories[indexPath.row]
            cell.configureCell(repo)
            return cell
        }
    }
}

extension RepositoriesViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        performSegueWithIdentifier("showPullRequests", sender: indexPath)
    }
}



