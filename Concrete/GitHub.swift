//
//  GitHub.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation
import Alamofire

class GitHub {
    
    
    static func getRepositories(page: Int, completion: (AnyObject?, NSError?) -> ()) {
        let url = NSURL(string: "https://api.github.com/search/repositories?q=language:Swift&sort=stars&page=\(page)")!
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        Alamofire.request(.GET, url).validate().responseJSON { (response) in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            switch response.result {
            case .Success:
                completion(response.result.value, nil)
            case .Failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
    static func getPullRequests(repository: Repository, completion: (AnyObject?, NSError?) -> ()) {
        let url = "https://api.github.com/repos/\(repository.owner!.username!)/\(repository.name!)/pulls?state=all"
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, url).validate().responseJSON { response in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            switch response.result {
            case .Success:
                completion(response.result.value, nil)
            case .Failure(let error):
                completion(nil, error)
            }
        }
    }

    
}