//
//  PullRequestsTableView.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequestsViewController: UIViewController {

    var repository: Repository! {
        didSet {
            navigationItem.title = repository.name
            
            GitHub.getPullRequests(repository) { (response, error) in
                if error != nil {
                    print(error)
                }
                let repos = Mapper<PullRequest>().mapArray(response)
                self.pullRequests.appendContentsOf(repos!)
                self.isLoading = false
                self.tableView.reloadData()
                
            }
        }
    }
    var pullRequests: [PullRequest] = []
    var isLoading = true
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var cellNib = UINib(nibName: TableViewCellIdentifiers.pullRequestCell, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.pullRequestCell)
        cellNib = UINib(nibName: TableViewCellIdentifiers.loadingCell, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.loadingCell)
        tableView.estimatedRowHeight = 108.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.automaticallyAdjustsScrollViewInsets = false
        
        
    }
}


extension PullRequestsViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoading {
            return 1
        }
        return self.pullRequests.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if isLoading {
            return tableView.dequeueReusableCellWithIdentifier(TableViewCellIdentifiers.loadingCell, forIndexPath: indexPath)
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(TableViewCellIdentifiers.pullRequestCell, forIndexPath: indexPath) as! PullRequestCell
        let pullRequest = pullRequests[indexPath.row]
        cell.configureCell(pullRequest)
        return cell
    }
}

extension PullRequestsViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let pr = pullRequests[indexPath.row]
        if let url = NSURL(string: pr.url!) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let opened = self.pullRequests.filter { $0.state == "open" }.count
        let closed = self.pullRequests.count - opened
        let view = NSBundle.mainBundle().loadNibNamed("SummaryView", owner: nil, options: nil).first as? SummaryView
        view?.configureSummaryView(opened, closedNumber: closed)
        return view
    }
}

