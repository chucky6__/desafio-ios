//
//  RepositoryCell.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit

class RepositoryCell: UITableViewCell {

    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var repoDescription: UILabel!
    @IBOutlet weak var forkNumber: UILabel!
    @IBOutlet weak var starsNumber: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    func configureCell(repository: Repository) {
        repoName.text = repository.name!
        repoDescription.text = repository.description!
        forkNumber.text = "\(repository.forks!)"
        starsNumber.text = "\(repository.stars!)"
        username.text = repository.owner?.username!
        userImageView.getUserImage(repository.owner!.imageURLString!)
    }
    
}
