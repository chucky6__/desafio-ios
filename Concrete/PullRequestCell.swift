//
//  PullRequestCell.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit



class PullRequestCell: UITableViewCell {
    
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestDescription: UILabel!
    @IBOutlet weak var pullRequestDate: UILabel!
    
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorUsername: UILabel!
    
    func configureCell(pullRequest: PullRequest) {
        pullRequestTitle.text = pullRequest.title
        pullRequestDescription.text = pullRequest.description
        pullRequestDate.text = configure(pullRequest.date!)
        authorUsername.text = pullRequest.owner?.username
        authorImageView.getUserImage((pullRequest.owner?.imageURLString!)!)
    }
    
    
    private func configure(date: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let pullRequestDate = dateFormatter.dateFromString(date)
        dateFormatter.dateStyle = .MediumStyle
        
        return dateFormatter.stringFromDate(pullRequestDate!)
    }
}
