//
//  Repositories.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation
import ObjectMapper

class Repositories: Mappable {
    var repositories: [Repository]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        repositories  <- map["items"]
    }
}