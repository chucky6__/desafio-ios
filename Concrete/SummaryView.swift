//
//  summaryView.swift
//  Concrete
//
//  Created by Antonio Alves on 5/10/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit

class SummaryView: UIView {

    @IBOutlet weak var summaryLabel: UILabel!
    
    func configureSummaryView(opened: Int, closedNumber closed: Int) {
        
        let closedFontColor = UIColor.blackColor()
        
        let closedAttributes = [NSForegroundColorAttributeName: closedFontColor]
        let openText = NSMutableAttributedString(string: "\(opened) opened", attributes: nil)
        let closedText = NSMutableAttributedString(string: " / \(closed) closed", attributes: closedAttributes)
        
        let summaryText = NSMutableAttributedString()
        summaryText.appendAttributedString(openText)
        summaryText.appendAttributedString(closedText)
        
        self.summaryLabel.attributedText = summaryText
        
    }

}
