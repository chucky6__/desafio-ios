//
//  TableViewCellIdentifiers.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation

struct TableViewCellIdentifiers {
    
    static let repositoryCell = "RepositoryCell"
    static let loadingCell = "Loading"
    static let pullRequestCell = "PullRequestCell"

}