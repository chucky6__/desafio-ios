//
//  Owner.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    var username: String?
    var imageURLString: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        username        <- map["login"]
        imageURLString  <- map["avatar_url"]
    }
}