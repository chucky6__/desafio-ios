//
//  UIImageView+DownloadImage.swift
//  Concrete
//
//  Created by Antonio Alves on 5/9/16.
//  Copyright © 2016 Antonio Alves. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

extension UIImageView {
    func getUserImage(urlString: String) {
        Alamofire.request(.GET, urlString).responseImage { [weak self] response in
            if let image = response.result.value {
                self?.image = image
            }
        }
    }
}
